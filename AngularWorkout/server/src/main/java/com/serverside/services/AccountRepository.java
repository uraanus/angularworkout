package com.serverside.services;

import com.serverside.model.Account;
import org.springframework.stereotype.Repository;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Repository
public class AccountRepository {

    private final Map<String, Account> accounts = new HashMap<>();
//    private final List<String[]> ConnectionIdAndSecretKey=new ArrayList<String[]>();
    private List<String> phoneNumbers=new ArrayList<String>();
    public void fillConnection() {
    	String[] temp=new String[2];
    	temp[0]="connection1";
    	
    }
    public Collection<Account> getAll() {
        return accounts.values();
    }

    public Account getByName(String name) {
        return accounts.get(name);
    }

    public void save(Account account) {
        accounts.put(account.getName(), account);
    }
    
    public Account checkConnection(Account a) {
    	AccountValidator av=new AccountValidator();
    	if(av.isValid(a)){
    		System.out.println("a is VAAALIIID");
    		return a;
    	}else 
    		System.out.println("a is NOOOOT VAAALIIID");
    		return null;
    	
    	
    
    }
    
    
    public boolean checkAccountName(Account a) {
    	
    	Account name=(accounts.get(a.getName()));
    	if(name == null)
    		return false;
    	return true;
    }
    
    public List<String> providePhoneNumbers(){
    	this.phoneNumbers.clear();
    	this.phoneNumbers.add("12345");
    	this.phoneNumbers.add("67890");
    	return this.phoneNumbers;
    	
    }
}
