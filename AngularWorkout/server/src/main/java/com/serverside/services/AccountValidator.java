package com.serverside.services;

import com.serverside.model.Account;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class AccountValidator {

    public boolean isValid(Account account) {
        try {
            UUID.fromString(account.getConnectionId());
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }
}
