package com.serverside;

import com.serverside.model.Account;
import com.serverside.services.AccountRepository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:4200")
@RestController

public class UiApiController {

	@Autowired
	AccountRepository accountRepository;

	@RequestMapping("/accounts")
	private List<Account> getAllAccounts() {
		System.out.println("the big length is " + accountRepository.getAll().size());
		return new ArrayList(accountRepository.getAll());

	}

	@RequestMapping("/create")
	private void createAccount(Account a) {

		accountRepository.save(a);
	}

	@PostMapping("/checkConnectionId")
	private Account checkforConnection(@RequestBody Account a) {
		Account _acc = new Account();
		System.out.println("Reciveeeeed");
		_acc = accountRepository.checkConnection(a);
		// System.out.println("print .. " +_acc.getConnectionId());
		return _acc;
	}

	@PostMapping("/getPhoneNumbers")
	private List<String> getPhoneNuumbers() {

		return this.accountRepository.providePhoneNumbers();
	}

	@PostMapping("/addAccount")
	private Account addAccount(@RequestBody Account a) {
		if (!accountRepository.checkAccountName(a)) {
			System.out.println("the name is " + a.getName());
			accountRepository.save(a);
			System.out.println("i will retuen object");
			System.out.println("the length from add is " + accountRepository.getAll().size());
			return a;
		}
		System.out.println("i iwll retien null");
		return null;

	}

}
