import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountListComponent } from '../account-list/account-list.component';
import { AddAccountComponent } from '../add-account/add-account.component';
import { AddAccountNameComponent } from '../add-account/add-account-name/add-account-name.component';



const routes: Routes = [
    { path: '', redirectTo: 'accounts', pathMatch: 'full' },
    { path: 'accounts', component: AccountListComponent },
    { path: 'createAccount', component: AddAccountComponent },
    { path: 'createAccountName', component: AddAccountNameComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule{

}