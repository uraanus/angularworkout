import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Account } from './account';
 


@Injectable({
    providedIn: 'root'
  })
export class AccountService{

    private serverURL = 'http://localhost:8080';

    constructor(private http: HttpClient) { }

    //geting all accounts
    getCustomersList(): Observable<any> {
        return this.http.get(`${this.serverURL}`+`/accounts`);
      }

      checkConnection(account : Account) : Observable<any> {
        console.log("from service " +account.secretKey);
        console.log("from service " +account.connectionId);
        return this.http.post(`${this.serverURL}`+`/checkConnectionId`,account);
      }


      getPhoneNumbersList(): Observable<any> {
        return this.http.post(`${this.serverURL}`+`/getPhoneNumbers`,null);
      }
      
      addAccount(account : Account) : Observable<any> {
        // console.log("from service " +account.secretKey);
         console.log("from service " +account.phoneNumber);
        return this.http.post(`${this.serverURL}`+`/addAccount`,account);
      }
}
