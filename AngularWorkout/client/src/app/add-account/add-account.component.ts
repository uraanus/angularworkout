import { Component, OnInit, OnChanges, SimpleChange, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AccountService } from '../shared/account.service';
import { Account } from '../shared/account';
import { Router } from '../../../node_modules/@angular/router';


@Component({
  selector: 'app-add-account',
  templateUrl: './add-account.component.html',
  styleUrls: ['./add-account.component.css']
})
export class AddAccountComponent implements OnInit {

  addAccountForm: FormGroup;
  submitted = false;
  buttonClicked = false;
  ac: Account = new Account();
  returnAccount: any;

  constructor(private formBuilder: FormBuilder, private accountService: AccountService, private r: Router) { }

  ngOnInit() {

    this.addAccountForm = new FormGroup({
      'connectionId': new FormControl(null, Validators.required),
      'secretKey': new FormControl(null, Validators.required)
    });
  }
 
  touchTheBox(test : any){
    this.buttonClicked=false;
    console.log("Here in touch box");
  }
  get f() { return this.addAccountForm.controls; }

  onSubmit() {  
   
    if (this.addAccountForm.invalid) {
      return;
    } else {
      
      this.accountService.checkConnection(this.ac).subscribe(resp => {
        this.returnAccount = resp;
        if (!this.returnAccount || typeof this.returnAccount.secretKey == 'undefined' || typeof this.returnAccount.connectionId == 'undefined') {
          console.log("ac is null");
          this.buttonClicked=true;
        } else {
          this.buttonClicked=false;
          this.submitted = true;
         
          this.r.navigate(['/createAccountName']);
          console.log("ac is NOT null");
          localStorage.setItem("initAccount",JSON.stringify(this.returnAccount));

        }
      })
      //console.log(this.returnAccount.connectionId);
      //console.log(this.returnAccount.secretKey);



    }

  }
}