
import { Component, OnInit, OnChanges, SimpleChange, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AccountService } from '../../shared/account.service';
import { Account } from '../../shared/account';
import { Router } from '../../../../node_modules/@angular/router';

@Component({
  selector: 'app-add-account-name',
  templateUrl: './add-account-name.component.html',
  styleUrls: ['./add-account-name.component.css']
})
export class AddAccountNameComponent implements OnInit {

  oldAccount : any;
  phones : any[];
  returnAccount: any;
  ac : Account;
  mappedAccount: Account = new Account();
  addAccountNameForm: FormGroup;
  nameExist : boolean=true;
  
  constructor(private formBuilder: FormBuilder, 
    private accountService: AccountService, private r : Router) { }


  ngOnInit() {
    this.addAccountNameForm = new FormGroup({
      'accountName': new FormControl(null, Validators.required),
      'phoneNumber': new FormControl(null, Validators.required)
    });
    this.oldAccount=localStorage.getItem("initAccount");
    this.ac=JSON.parse(this.oldAccount);
    this.getPhoneNumbersList();
    this.mappedAccount=this.ac;
    //console.log(this.ac.connectionId);
  }


  get f() { return this.addAccountNameForm.controls; }

  getPhoneNumbersList(){
    
    this.accountService.getPhoneNumbersList().subscribe(
      res=>{
        this.phones=res
      }
    )
    
  }

  
  onSubmit() {  
    if (this.addAccountNameForm.invalid) {
      return;
    }else {
      console.log("mapped account is " + this.mappedAccount.phoneNumber);
      this.accountService.addAccount(this.mappedAccount).subscribe(
        res=>{
          this.returnAccount=res;
        }
      )
      
    }
    console.log("the return account is "+this.returnAccount);

    if(!this.returnAccount || typeof this.returnAccount == 'undefined'){
      this.nameExist=false;
      return;
    }else {
       this.r.navigate(['/accounts']);
    }
    }

  

}
