import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAccountNameComponent } from './add-account-name.component';

describe('AddAccountNameComponent', () => {
  let component: AddAccountNameComponent;
  let fixture: ComponentFixture<AddAccountNameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAccountNameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAccountNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
