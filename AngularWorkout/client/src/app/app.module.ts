import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AccountListComponent } from './account-list/account-list.component';
import { AppRoutingModule } from './shared/app.routing.module';
import { HttpClientModule } from '../../node_modules/@angular/common/http';
import { AddAccountComponent } from './add-account/add-account.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AddAccountNameComponent } from './add-account/add-account-name/add-account-name.component';

@NgModule({
  declarations: [
    AppComponent,
    AccountListComponent,
    AddAccountComponent,
    AddAccountNameComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
